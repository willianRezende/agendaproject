//https://github.com/agenda/agendash/blob/master/test/test-express.js
//https://www.npmjs.com/package/agenda#agenda-events
//https://www.npmjs.com/package/agenda-rest
//https://github.com/agenda/agendash
//https://medium.com/@vaibhavhpatil/node-js-agenda-sheduling-job-easy-24a640cd588d
//https://thecodebarbarian.com/node.js-task-scheduling-with-agenda-and-mongodb
//https://techdai.info/agenda-and-agendash-for-scheduling-in-node-js-with-mongodb/
// tips for agenda https://jasonbutz.info/2018/12/nodejs-agenda/

const express = require('express'); 
const app = express(); 

const Agenda = require('agenda');
const Agendash = require('agendash');

const agenda = new Agenda({ db: { address : 'mongodb://127.0.0.1/agendaDb' },
  processEvery: '30 seconds'
}); 

agenda.define('say hello', (job,done) => {
  erro = true;
  if (erro) {
    throw "Test error in queue"; 
  } else {
    console.log('Hello!');
    done();
  }
});

agenda.define('send mail', (job,done) => {
  if (erro) {
    throw "Test error in queue"; 
  } else {
    console.log('Send Mail!');
    done();
  }
});

app.get('/service', (req, res) => {
  agenda.every
}); 

agenda.on( "ready", function() {
  try { 
    agenda.every('30 seconds', 'say hello'); 
    agenda.every('3 minute', 'send mail');  
    agenda.start()
    //execute task now 
    //agenda.now( "beerRun" );
  } catch (err) {
    console.log(err);
  }
});


agenda.on('complete', job => {
  console.log(`Job ${job.attrs.name} finished`);
});

agenda.on('fail:send email', (err, job) => {
  console.log(`Job failed with error: ${err.message}`);
});

app.use('/dash', Agendash(agenda));

app.listen(3000, () => console.log('Server is running...')); 