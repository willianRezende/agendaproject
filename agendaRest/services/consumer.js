const Agenda = require('agenda');
const Agendash = require('agendash');
const express = require('express'); 
const app = express(); 

const agenda = new Agenda({
      db: { address : 'mongodb://127.0.0.1/agendaDb' },  
      processEvery: '30 seconds'
});

agenda.start()

app.use('/dash', Agendash(agenda));

app.listen(3000, () => console.log('Server is running...')); 

