const Agenda = require('agenda');

const agenda = new Agenda({
      db: { address : 'mongodb://127.0.0.1/agendaDb' },  
      processEvery: '30 seconds'
});

agenda.define('say hello', (job,done) => {
  console.log('Hello!');
  done();
});


agenda.on( "ready", function() {
  try { 
    agenda.every('2 minutes', 'say hello'); 
    agenda.start({processEvery: '3 minutes'});
  } catch (err) {
    console.log(err);
  }
});