const express = require('express'); 
const app = express(); 
const JobService = require('./JobDefine');

const database = require('./database.json')
const Agendash = require('agendash');
const agenda = require('./agenda');
/*const agenda = new Agenda({
  db: { address : 'mongodb://127.0.0.1/agendaDb' },  
  processEvery: '30 seconds'
});*/

function init(){
  console.log(agenda);    
  console.log(agenda.jobs())
  agenda.on( "ready", function() {
    try { 
      database.map(job => {
        JobService.defineJob(agenda, job);
      });
      //agenda.every('30 seconds', 'say hello'); 
      //agenda.every('30 seconds', 'First job');  
      agenda.start()
      //execute task now 
      //agenda.now( "beerRun" );
    } catch (err) {
      console.log(err);
    }
  });
  app.use(express.json());

  app.post('/job', (req, res) => {
    const job = req.body;
    console.log(job);
    try {
      JobService.defineJob(agenda, job);
      res.json({job: 'OK'});
    } catch (err) {
      res.json({error: err});
    }  
  }); 

function createJob(job) {
    console.log(agenda);
    JobService.defineJob(agenda,job);
  }

  agenda.on('complete', job => {
    console.log(`Job ${job.attrs.name} finished`);
  });

  agenda.on('fail:send email', (err, job) => {
    console.log(`Job failed with error: ${err.message}`);
  });


  app.use('/dash', Agendash(agenda));

  app.listen(3000, () => console.log('Server is running...')); 
}

function createJob(job){
  JobService.defineJob(agenda, job);
}

module.exports = {
  init, 
  createJob
}