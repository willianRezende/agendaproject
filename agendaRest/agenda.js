const Agenda = require('agenda');

module.exports = new Agenda({
      db: { address : 'mongodb://127.0.0.1/agendaDb' },  
      processEvery: '30 seconds'
  });